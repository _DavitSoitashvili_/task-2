<?php

use App\Http\Controllers\ArticleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/articles', [ArticleController::class, "index"])->name("articles");
Route::get('/article/create', [ArticleController::class, "create"])->name("create");
Route::post('/article/store', [ArticleController::class, "store"])->name("article.store");
Route::get('/article/update/{id}', [ArticleController::class, "update"])->name("article.update");
Route::put('/article/store_updated/{id}', [ArticleController::class, "storeUpdated"])->name("article.store_updated");
Route::delete('/article/delete/{id}', [ArticleController::class, "delete"])->name("article.delete");
