@extends("layouts.layout")
@section("Title")
    Update Article
@endsection
@section("Content")

    <form method="post" action="{{route("article.store_updated", $article->id)}}">
        @csrf
        @method("PUT")
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Title</label>
            <input required="required" name="title" type="text" class="form-control" value="{{$article->title}}"
                   aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Text</label>
            <input required="required" name="text_body" type="text" class="form-control" value="{{$article->text_body}}"
                   aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Author</label>
            <input required="required" name="author" type="text" class="form-control" value="{{$article->author}}"
                   aria-describedby="emailHelp">
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection
