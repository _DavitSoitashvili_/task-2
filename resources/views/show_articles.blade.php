@extends("layouts.layout")
@section("Title")
    Show Articles
@endsection
@section("Content")
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Title</th>
            <th scope="col">Body Text</th>
            <th scope="col">Author</th>
        </tr>
        </thead>
        <tbody>
        @foreach($articles as $article)
            <tr>
                <th style="font-size: 17px" scope="row">{{$article->id}}
                    <div style="display: flex">
                        <form method="get" action="{{route("article.update", $article->id)}}">
                            <button style="width:50px" class="btn btn-success" type="submit">Edit</button>
                        </form>
                        <form method="post" action="{{route("article.delete", $article->id)}}">
                            @csrf
                            @method("DELETE")
                            <button style="width:50px; margin-left: 5px" class="btn btn-danger" type="submit">-</button>
                        </form>
                    </div>
                </th>
                <td style="font-size: 18px">{{$article->title}}</td>
                <td style="font-size: 18px">{{$article->text_body}}</td>
                <td style="font-size: 18px">{{$article->author}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display: flex; justify-items: right">
        <button style="width: 50px; margin-left: 5px; margin-bottom: 10px" class="btn btn-primary"><a style="color: white; text-decoration: none"
                                                                                 href="{{route("create")}}">+</a>
        </button>
    </div>
    {{$articles->links()}}
@endsection
