<?php


namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;


class ArticleController extends BaseController
{
    public function index()
    {
        $articles = Article::paginate(10);
        return View("show_articles", compact("articles"));
    }


    public function create()
    {

        return View("create_article");
    }

    public function store(Request $request)
    {
        $article = new Article();
        $article->title = $request->get("title");
        $article->text_body = $request->get("text_body");
        $article->author = $request->get("author");
        $article->save();
        $articles = Article::paginate(10);
        return View("show_articles", compact("articles"));

    }

    public function update($id)
    {
        $article = Article::findOrFail($id);

        return View("update_article", compact("article"));

    }

    public function storeUpdated(Request $request, $id)
    {
        $article = Article::findOrFail($id);
        $article->update($request->all());
        $articles = Article::paginate(10);
        return View("show_articles", compact("articles"));
    }

    public function delete($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();
        $articles = Article::paginate(10);
        return View("show_articles", compact("articles"));

    }

}
